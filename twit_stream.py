import requests
import json
import time
from time import mktime
from datetime import datetime
import pytz

def pretty_date(time=False):
    """
    Get a datetime object or a int() Epoch timestamp and return a
    pretty string like 'an hour ago', 'Yesterday', '3 months ago',
    'just now', etc
    """
    now = datetime.now(pytz.timezone('UTC'))
    if type(time) is int:
        diff = now - datetime.fromtimestamp(time)
    elif isinstance(time,datetime):
        diff = now - time
    elif not time:
        diff = now - now
    second_diff = diff.seconds
    day_diff = diff.days

    if day_diff < 0:
        return ''

    if day_diff == 0:
        if second_diff < 10:
            return "just now"
        if second_diff < 60:
            return str(second_diff) + " seconds ago"
        if second_diff < 120:
            return  "a minute ago"
        if second_diff < 3600:
            return str( second_diff / 60 ) + " minutes ago"
        if second_diff < 7200:
            return "an hour ago"
        if second_diff < 86400:
            return str( second_diff / 3600 ) + " hours ago"
    if day_diff == 1:
        return "Yesterday"
    if day_diff < 7:
        return str(day_diff) + " days ago"
    if day_diff < 31:
        return str(day_diff/7) + " weeks ago"
    if day_diff < 365:
        return str(day_diff/30) + " months ago"
    return str(day_diff/365) + " years ago"

def stream():
    print "Requesting stream"
    return requests.post('https://stream.twitter.com/1.1/statuses/filter.json',
        data={'track': 'singapore'}, auth=('acsgbot', '!QAZ0pl,tt'), prefetch=False)

if __name__ == '__main__':
    r = stream()
    for line in r.iter_lines():
        if line: # filter out keep-alive new lines
            data = json.loads(line)
            print data.keys()
            print data
            break
            print 'Screen name: ', data[u'user'][u'screen_name']
            print 'Profile image url: ', data[u'user'][u'profile_image_url']
            #dt =  datetime.fromtimestamp(mktime(time.strptime(data[u'created_at'],'%a %b %d %H:%M:%S +0000 %Y')))
            #dt = pytz.timezone('UTC').localize(dt)
            #sgt_dt = dt.astimezone(pytz.timezone('Asia/Singapore'))
            #print pretty_date(dt)
            #print 'Text: ', data[u'text']
