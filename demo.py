from flask import Flask, jsonify, render_template
from werkzeug.routing import BaseConverter
import time, pytz
app = Flask(__name__)

class RegexConverter(BaseConverter):
    def __init__(self, url_map, *items):
        super(RegexConverter, self).__init__(url_map)
        self.regex = items[0]


app.url_map.converters['regex'] = RegexConverter

@app.route("/", methods=['GET'])
def index():
    return render_template("index.html")

@app.route("/message", methods=['GET'])
def message():
    return jsonify(message="A message")

@app.route("/wordcloud/<topic>", methods=['GET'])
def wordcloud_data(topic):
    return jsonify(data={})

@app.route('/<regex("stream_update.*"):updateid>')
def twit_stream_update(updateid):
    print "Waiting for new twit"
    while not app.config['new_data_available']:
        time.sleep(0.5)
    app.config['new_data_available'] = False
    print "Data available"
    return jsonify(message="updated")

@app.route('/<regex("stream_data.*"):dataid>')
def twit_stream_date(dataid):
    res = jsonify(app.config['stream_thread'].get_data())
    return res

import threading
from collections import deque
import twit_stream
from twit_stream import pretty_date
import json
from time import mktime
from datetime import datetime
DEQUE_MAX_LENGTH = 25
class StreamThread(threading.Thread):
    def __init__(self, deque):
        threading.Thread.__init__(self)
        self.deque = deque
        self._stop = threading.Event()

    def stop(self):
        self._stop.set()

    def stopped(self):
        return self._stop.isSet()

    def get_data(self):
        if len(self.deque) == 0:
            return {}
        data = self.deque.pop()
        self.deque.append(data)
        return data

    def run2(self):
        while True:
            time.sleep(5)
            if self.stopped():
                print "Thread stopped"
                return
            #print "New iteration:"
            self.deque.append({u'screen_name': 'abc', u'profile_image_url':
                'adef', u'text': 'dfs', u'date': 'dsfsd'})

            app.config['new_data_available'] = True

            # Keeping maximum of DEQUE_MAX_LENGTH elements
            if len(self.deque) == DEQUE_MAX_LENGTH:
                self.deque.popleft()

    def run(self):
        print "Waiting for data from stream\n"
        self.stream = twit_stream.stream()
        for line in self.stream.iter_lines():
            if self.stopped():
                return
            if line:
                data = json.loads(line)
                if 'disconnect' in data:
                    return
                # Parsing date
                dt =  datetime.fromtimestamp(mktime(time.strptime(data[u'created_at'],'%a %b %d %H:%M:%S +0000 %Y')))
                dt = pytz.timezone('UTC').localize(dt)
                #sgt_dt = dt.astimezone(pytz.timezone('Asia/Singapore'))
                relative_date = pretty_date(dt)

                text = data[u'text']
                screen_name = data[u'user'][u'screen_name']
                profile_image_url =  data[u'user'][u'profile_image_url']
                self.deque.append({u'screen_name': screen_name, u'profile_image_url':
                    profile_image_url, u'text': text, u'date': relative_date})

                app.config['new_data_available'] = True

                # Keeping maximum of DEQUE_MAX_LENGTH elements
                if len(self.deque) == DEQUE_MAX_LENGTH:
                    self.deque.popleft()

if __name__ == "__main__":
    t = StreamThread(deque())
    t.start()
    app.debug = True
    app.config['new_data_available'] = False
    app.config['stream_thread'] = t
    try:
        print "Start to run app\n"
        app.run(threaded=True)
    except (RuntimeError, TypeError, NameError) as e:
        print e.value
    finally:
        t.stop()
        print "Waiting for twit stream thread to stop"
        t.join()

