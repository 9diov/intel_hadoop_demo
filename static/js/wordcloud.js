$(document).ready(function() {
	function update_wordcloud(party) {
		d3.select("svg#wordcloud_" + party).remove()
		var fontSize = d3.scale.log().range([6, 36]);
		d3.layout.cloud().timeInterval(10)
		.size([450, 450])
		.words($.wordcloud[party])
		//.rotate(function() { return ~~(Math.random() * 2) * 90; })
		//.rotate(function() { return ~~(Math.random() * 5) * 30 - 60; })
		.rotate(function() { return 0; })
		.font("Impact")
		.fontSize(function(d) { return fontSize(+d.size); })
		.on("end", draw)
		.start();

		function draw(words) {
			var vis = d3.select("#"+party+"_div").append("svg:svg").attr("id", function() { return "wordcloud_" + party; })
			//var vis = d3.select("#wordcloud_" + party)
			.attr("class", function() { return "wordcloud"; })
			.append("g")
			.attr("transform", "translate(225,225)");
			var text = vis.selectAll("text").data(words);

			text.transition().duration(1000)
			.attr("transform", function(d) {
				return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
			}).style("font-size", function(d) {
				return d.size + "px";
			});

			text.enter().append("text")
			.style("font-size", function(d) { return d.size + "px"; })
			.style("font-family", "Impact")
			.style("fill", function(d, i) { return fill(i); })
			.attr("text-anchor", "middle")
			.transition().duration(1000)
			.attr("transform", function(d) {
				return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
			})
			.text(function(d) { return d.text; });
		}
	}
	$.wordcloud = {
		pap: [],
		rp: [],
		sda: [],
		wp: []
	};
	function getWordCloudData(url, party) {
		$.ajax({
			type: 'GET',
			url: url,
			dataType: 'json',
			success: function(data) {
				$.wordcloud[party] = data;
				update_wordcloud(party);
			},
			data: {}
		});
	}

	var fill = d3.scale.category20();
    getWordCloudData('/static/data/wordCloudPAP.json', 'pap');
    getWordCloudData('/static/data/wordCloudRP.json', 'rp');
    getWordCloudData('/static/data/wordCloudRP.json', 'sda');
    getWordCloudData('/static/data/wordCloudRP.json', 'wp');
	$(".update_wordcloud_btn").click(function(event) {
		event.preventDefault();

		if ($(this).attr('id') == 'update_pap_btn') {
			getWordCloudData('/static/data/wordCloudPAP.json', 'pap');
		}
		else if ($(this).attr('id') == 'update_rp_btn') {
			getWordCloudData('/static/data/wordCloudRP.json', 'rp');
		}
		else if ($(this).attr('id') == 'update_sda_btn') {
			getWordCloudData('/static/data/wordCloudRP.json', 'sda');
		}
		else if ($(this).attr('id') == 'update_wp_btn') {
			getWordCloudData('/static/data/wordCloudRP.json', 'wp');
		}
	});
});
