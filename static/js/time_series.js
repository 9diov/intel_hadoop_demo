$(document).ready(function() {
	function sentimentGraph() {
		d3.json("/static/data/timeseries.json", function(data) {
			nv.addGraph(function() {
				var chart = nv.models.lineWithFocusChart();
				//var chart = nv.models.lineChart();

				chart.xAxis.tickFormat(function(d) {
					return d3.time.format('%x')(new Date(d));
				});
				chart.x2Axis.tickFormat(function(d) {
					return d3.time.format('%x')(new Date(d));
				});

				chart.yAxis.tickFormat(d3.format(',.02f'));
				chart.y2Axis.tickFormat(d3.format(',.2f'));

				d3.select('#timeseries svg').datum(data)
				.transition().duration(500)
				.call(chart);

				nv.utils.windowResize(chart.update);

				return chart;
			});
		});
	}

	setTimeout(function() {
		sentimentGraph();
	}, 5000);

	$("#update_timeseries").click(function(event) {
		event.preventDefault();

		d3.select('#timeseries svg').remove();
		d3.select('#timeseries').append('svg:svg');
		sentimentGraph();
	});

});
