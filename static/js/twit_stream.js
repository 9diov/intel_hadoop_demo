function load_twit() {
	$.twit_stream.failure = false;
	$.ajax({ url: '/stream_data&amp;'+Math.floor((Math.random()*100)+1),
		   success: function(data) {
				$.twit_stream.cur_tweets += 1;
				//var newDate = new Date();
				//dateString = newDate.toUTCString();
				//$("#twit-stream").append("<p>" + dateString + ": " + JSON.stringify(data) + "</p>");
				twit_content = "<p id='msg-" + $.twit_stream.cur_tweets + "'><img src='"+ data.profile_image_url + "'>" +"<span class='msg-block'><strong>"+data.screen_name+"</strong><span class='time'>, " + data.date + "</span>:" +"<span class='msg'>"+data.text+"</span></span></p>";
				if ($.twit_stream.no_tweets == 0) {
					$("#twit-stream").html(twit_content);
				} else if ($.twit_stream.no_tweets == 5) {
					$("#msg-" + ($.twit_stream.cur_tweets - 5)).slideUp(800, function() {
						$(this).remove();
					});
					$.twit_stream.no_tweets -= 1;
					$("#twit-stream").append(twit_content);
				} else {
					$("#twit-stream").append(twit_content);
				}
				$.twit_stream.no_tweets += 1;
				$("#msg-" + $.twit_stream.cur_tweets).hide().fadeIn(800);
		   },
		   cache: false
	});
}
function load_data() {
	if($.twit_stream.failure) {
		setTimeout(function() { 
			$.twit_stream.failure = false;
			load_data(); 
		}, 15000);
		return;
	}
	$.ajax({ url: '/stream_update&amp;'+Math.floor((Math.random()*100)+1),
		   success: load_twit,
		   error: function() { $.twit_stream.failure = true;},
		   complete: function() { setTimeout(load_data, 1000); },
		   timeout: 60000,
		   cache: false
	});
}

$(document).ready(function() {
	$.twit_stream = {
		max_tweets: 10,
		cur_tweets: 0,
		no_tweets: 0,
		failure: false
	}
	$("#twit-stream").append("awaiting data...");
	load_data();
});
