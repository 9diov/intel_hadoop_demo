import csv
import json

import dateutil.parser
import time
def parse_time(datestring):
    dt = dateutil.parser.parse(datestring)
    return time.mktime(dt.timetuple())*1000

def parse_data(csvfile):
    count = 0
    data = []
    with open(csvfile, 'rb') as f:
        reader = csv.reader(f)
        for row in reader:
            count += 1
            if count == 1:
                continue
            #if count == 300:
                #break
            data.append({"x": parse_time(row[2].replace(" ", "T")), "y": float(row[9])})
            #data.append({"x": count, "y": float(row[9])})
    data = data[1:]
    return data

def convert(party, output):
    output.append({"key": party.upper(), "values": parse_data(party + ".csv")})


if __name__ == "__main__":
    output = []
    convert("pap", output)
    convert("rp", output)
    convert("sda", output)
    convert("wp", output)

    with open('timeseries.json', 'w') as f:
        json.dump(output, f)
