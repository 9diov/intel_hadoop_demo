import csv
import json

def convert_to_json(csvfile):
    data = []
    with open(csvfile, 'rb') as f:
        reader = csv.reader(f)
        for row in reader:
            data.append({"text":row[1], "size": row[2]})
    data = data[1:]
    data.sort(key=lambda elem: int(elem["size"]))
    return data[-50:]

def convert(party):
    with open('wordCloud'+party+'.json', 'w') as f:
        json.dump(convert_to_json('wordCloud'+party+'.csv'), f)

if __name__ == '__main__':
    convert('PAP')
    convert('RP')
    convert('SDA')
    convert('WP')



